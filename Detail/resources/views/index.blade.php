<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('images/favicon.jpg') }}">

    <!-- CSFR token for ajax call -->
    <meta name="_token" content="{{ csrf_token() }}"/>

    <title>Manage Employee</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- icheck checkboxes -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/yellow.css">

    <!-- toastr notifications -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">


    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        .panel-heading {
            padding: 0;
        }
        .panel-heading ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
        }
        .panel-heading li {
            float: left;
            border-right:1px solid #bbb;
            display: block;
            padding: 14px 16px;
            text-align: center;
        }
        .panel-heading li:last-child:hover {
            background-color: #ccc;
        }
        .panel-heading li:last-child {
            border-right: none;
        }
        .panel-heading li a:hover {
            text-decoration: none;
        }

        .table.table-bordered tbody td {
            vertical-align: baseline;
        }
        /* icheck checkboxes */
        .iradio_flat-yellow {
            background: url(https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/yellow.png) no-repeat;
        }
    </style>

</head>

<body>
    <div class="col-md-8 col-md-offset-2">
        <h2 class="text-center">Manage Employee</h2>
        <br />
        <div class="panel panel-default">
            <div class="panel-heading">
                <ul>
                    <li><i class="fa fa-file-text-o"></i> All the current Employee</li>
                    <a href="#" class="add-modal"><li>Add a Employee</li></a>
                    <a href="{{ url('/excelgen') }}"><li>GET ALL STUDENT DATA IN EXCEL</li></a>
                    <a href="{{ url('/pdfgen') }}"><li>GET ALL STUDENT DATA IN PDF</li></a>
                    <a href="{{ url('/sendmail') }}"><li>SEND MAIL</li></a>
                    <input type="text" name="search_txt"/>
                        &nbsp;&nbsp;&nbsp;
                    <Button class="search-modal" type="submit" name="search_btn" id="search" value="SEARCH">SEARCH</Button>
                </ul>
            </div>

            <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover" id="postTable" style="visibility: hidden;">
                        <thead>
                            <tr>
                                <th valign="middle">@sortablelink('id')</th>
                                <th>@sortablelink('fname')</th>
                                <th>@sortablelink('lname')</th>
                                <th>@sortablelink('email')</th>
                                <th>@sortablelink('contact')</th>
                                <th>Attendence</th>
                                <th>Last Updated</th>
                                <th>Actions</th>
                            </tr>
                            {{ csrf_field() }}
                        </thead>
                        <tbody>
                            @if(!empty($employee))
                            @foreach($employee as $employee)
                                <tr class="item{{$employee->id}} @if($employee->attendence) warning @endif">
                                    <td>{{$employee->id}}</td>
                                    <td>{{$employee->fname}}</td>
                                    <td>{{$employee->lname}}</td>
                                    <td>{{$employee->email}}</td>
                                    <td>{{$employee->contact}}</td>
                                    <td class="text-center"><input type="checkbox" class="published" data-id="{{$employee->id}}" @if ($employee->attendence) checked @endif></td>
                                    <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $employee->updated_at)->diffForHumans() }}</td>
                                    <td>
                                        <button class="show-modal btn btn-success" data-id="{{$employee->id}}" 
                                            data-fname="{{$employee->fname}}" 
                                            data-lname="{{$employee->lname}}" 
                                            data-email="{{$employee->email}}" 
                                            data-contact="{{$employee->contact}}">
                                        <span class="glyphicon glyphicon-eye-open"></span>
                                        Show
                                        </button>
                                        <button class="edit-modal btn btn-info" data-id="{{$employee->id}}" 
                                            data-fname="{{$employee->fname}}" 
                                            data-lname="{{$employee->lname}}"
                                            data-email="{{$employee->email}}" 
                                            data-contact="{{$employee->contact}}">
                                        <span class="glyphicon glyphicon-edit"></span> 
                                        Edit
                                        </button>
                                        <button class="delete-modal btn btn-danger" data-id="{{$employee->id}}" 
                                            data-fname="{{$employee->fname}}" 
                                            data-lname="{{$employee->lname}}" 
                                            data-email="{{$employee->email}}" 
                                            data-contact="{{$employee->contact}}">
                                        <span class="glyphicon glyphicon-trash"></span> 
                                        Delete
                                        </button>
                                        <a href="{{ url('/exportexcelsingle',[$employee->id]) }}"><button>EXCEL</button>
                                        </a>
                                        <a href="{{ url('/generatepdfsingle',[$employee->id]) }}"><button>PDF</button></a>
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
            </div><!-- /.panel-body -->
        </div><!-- /.panel panel-default -->
    </div><!-- /.col-md-8 -->

    <!-- Modal form to add a post -->
    <div id="addModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="fname">Fname:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="fname_add" autofocus>
                                <small>Min: 2, Max: 32, only text</small>
                                <p class="errorTitle text-center alert alert-danger hidden"></p>
                            </div>
                            <label class="control-label col-sm-2" for="lname">Lname:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="lname_add" autofocus>
                                <small>Min: 2, Max: 32, only text</small>
                                <p class="errorTitle text-center alert alert-danger hidden"></p>
                            </div>

                            <label class="control-label col-sm-2" for="email">Email:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="email_add" autofocus>
                                <small>only email</small>
                                <p class="errorTitle text-center alert alert-danger hidden"></p>
                            </div>

                            <label class="control-label col-sm-2" for="contact">Contact:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="contact_add" autofocus>
                                <small>Max: 10, only number</small>
                                <p class="errorTitle text-center alert alert-danger hidden"></p>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success add" data-dismiss="modal">
                            <span id="" class='glyphicon glyphicon-check'></span> Add
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal form to show a post -->
    <div id="showModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="id">ID:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="id_show" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="fname">Fname:</label>
                            <div class="col-sm-10">
                                <input type="name" class="form-control" id="fname_show" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="lname">Lname:</label>
                            <div class="col-sm-10">
                                <input type="name" class="form-control" id="lname_show" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email">Email:</label>
                            <div class="col-sm-10">
                                <input type="name" class="form-control" id="email_show" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="contact">Contact:</label>
                            <div class="col-sm-10">
                                <input type="name" class="form-control" id="contact_show" disabled>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal form to edit a form -->
    <div id="editModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="id">ID:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="id_edit" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="fname">Fname:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="fname_edit" autofocus>
                                <p class="errorTitle text-center alert alert-danger hidden"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="lname">Lname:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="lname_edit" autofocus>
                                <p class="errorTitle text-center alert alert-danger hidden"></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email">Email:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="email_edit" autofocus>
                                <p class="errorTitle text-center alert alert-danger hidden"></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="contact">Contact:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="contact_edit" autofocus>
                                <p class="errorTitle text-center alert alert-danger hidden"></p>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary edit" data-dismiss="modal">
                            <span class='glyphicon glyphicon-check'></span> Edit
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal form to delete a form -->
    <div id="deleteModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h3 class="text-center">Are you sure you want to delete the following post?</h3>
                    <br />
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="id">ID:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="id_delete" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="fname">Fname:</label>
                            <div class="col-sm-10">
                                <input type="name" class="form-control" id="fname_delete" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="lname">Lname:</label>
                            <div class="col-sm-10">
                                <input type="name" class="form-control" id="lname_delete" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email">Email:</label>
                            <div class="col-sm-10">
                                <input type="name" class="form-control" id="email_delete" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="contact">Contact:</label>
                            <div class="col-sm-10">
                                <input type="name" class="form-control" id="contact_delete" disabled>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                            <span id="" class='glyphicon glyphicon-trash'></span> Delete
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

    <!-- Bootstrap JavaScript -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.1/js/bootstrap.min.js"></script>

    <!-- toastr notifications -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <!-- icheck checkboxes -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>

    <!-- Delay table load until everything else is loaded -->
    <script>
        $(window).load(function(){
            $('#postTable').removeAttr('style');
        })
    </script>


    <script>
        $(document).on('click', '.search-modal', function(){
            alert($('input[name=search_txt]').val());

            $.ajax({
                type: 'POST',
                url: '/search',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'fname': $('input[name=search_txt]').val()
                },
                success:function(data){
                    alert(data);
                    var rows="";
                    $.each(data,function(key,value){
                        /*alert(value.id);
                        alert(value.fname);
                        alert(value.lname);
                        alert(value.email);
                        alert(value.contact);*/

                        rows = rows + '<tr>';
                        rows = rows + '<td>'+value.id+'</td>';
                        rows = rows + '<td>'+value.fname+'</td>';
                        rows = rows + '<td>'+value.lname+'</td>';
                        rows = rows + '<td>'+value.email+'</td>';
                        rows = rows + '<td>'+value.contact+'</td></tr>';
                    });

                    $("tbody").html(rows);
                },
        });
    });
    </script>




    <script>
        $(document).ready(function(){
            $('.published').iCheck({
                checkboxClass: 'icheckbox_square-yellow',
                radioClass: 'iradio_square-yellow',
                increaseArea: '20%'
            });
            $('.published').on('ifClicked', function(event){
                id = $(this).data('id');
                $.ajax({
                    type: 'POST',
                    url: "{{ URL::route('changeStatus') }}",
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': id
                    },
                    success: function(data) {
                        // empty
                    },
                });
            });
            $('.published').on('ifToggled', function(event) {
                $(this).closest('tr').toggleClass('warning');
            });
        });

    </script>

    <!-- AJAX CRUD operations -->
    <script type="text/javascript">
        // add a new post
        $(document).on('click', '.add-modal', function() {
            $('.modal-title').text('Add');
            $('#addModal').modal('show');
        });
        $('.modal-footer').on('click', '.add', function() {
            $.ajax({
                type: 'POST',
                url: 'employee',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'fname': $('#fname_add').val(),
                    'lname': $('#lname_add').val(),
                    'email': $('#email_add').val(),
                    'contact': $('#contact_add').val()
                },
                success: function(data) {
                    $('.errorTitle').addClass('hidden');
                    $('.errorContent').addClass('hidden');

                    if ((data.errors)) {
                        setTimeout(function () {
                            $('#addModal').modal('show');
                            toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                        }, 500);

                        if (data.errors.title) {
                            $('.errorTitle').removeClass('hidden');
                            $('.errorTitle').text(data.errors.title);
                        }
                        if (data.errors.content) {
                            $('.errorContent').removeClass('hidden');
                            $('.errorContent').text(data.errors.content);
                        }
                    } else {
                        toastr.success('Successfully added employee!', 'Success Alert', {timeOut: 5000});
                        $('#postTable').append("<tr class='item" + 
                            data.id +"'><td>" + 
                            data.id +"</td><td>" + 
                            data.fname +"</td><td>" + 
                            data.lname +"</td><td>" + 
                            data.email + "</td><td>" + 
                            data.contact + 
                            "</td><td class='text-center'><input type='checkbox' class='new_published' data-id='" + 
                            data.id + 
                            " '></td><td>Right now</td><td><button class='show-modal btn btn-success' data-id='" + 
                            data.id + "' data-fname='" + 
                            data.fname + "' data-lname='" + 
                            data.lname + "'data-email='" + 
                            data.email + "'data-contact='" + 
                            data.contact + 
                            "'><span class='glyphicon glyphicon-eye-open'></span> Show</button> <button class='edit-modal btn btn-info' data-id='" +data.id + 
                            "' data-fname='" +data.fname + 
                            "' data-lname='" +data.lname + 
                            "'data-email='" +data.email + 
                            "'data-contact='" +data.contact + 
                            "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + 
                            data.id + "' data-fname='" + 
                            data.fname + "' data-lname='" + 
                            data.lname + "'data-email='" + 
                            data.email + "'data-contact='" + 
                            data.contact + "'><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
                        $('.new_published').iCheck({
                            checkboxClass: 'icheckbox_square-yellow',
                            radioClass: 'iradio_square-yellow',
                            increaseArea: '20%'
                        });
                        $('.new_published').on('ifToggled', function(event){
                            $(this).closest('tr').toggleClass('warning');
                        });
                        $('.new_published').on('ifChanged', function(event){
                            id = $(this).data('id');
                            $.ajax({
                                type: 'POST',
                                url: "{{ URL::route('changeStatus') }}",
                                data: {
                                    '_token': $('input[name=_token]').val(),
                                    'id': id
                                },
                                success: function(data) {
                                    // empty
                                },
                            });
                        });
                    }
                },
            });
        });

        // Show a post
        $(document).on('click', '.show-modal', function() {
            $('.modal-title').text('Show');
            $('#id_show').val($(this).data('id'));
            $('#fname_show').val($(this).data('fname'));
            $('#lname_show').val($(this).data('lname'));
            $('#email_show').val($(this).data('email'));
            $('#contact_show').val($(this).data('contact'));
            $('#showModal').modal('show');
        });


        // Edit a post
        $(document).on('click', '.edit-modal', function() {
            $('.modal-title').text('Edit');
            $('#id_edit').val($(this).data('id'));
            $('#fname_edit').val($(this).data('fname'));
            $('#lname_edit').val($(this).data('lname'));
            $('#email_edit').val($(this).data('email'));
            $('#contact_edit').val($(this).data('contact'));
            id = $('#id_edit').val();
            $('#editModal').modal('show');
        });
        $('.modal-footer').on('click', '.edit', function() {
            $.ajax({
                type: 'PUT',
                url: 'employee/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': $("#id_edit").val(),
                    'fname': $('#fname_edit').val(),
                    'lname': $('#lname_edit').val(),
                    'email': $('#email_edit').val(),
                    'contact': $('#contact_edit').val()
                },
                success: function(data) {
                    $('.errorTitle').addClass('hidden');
                    $('.errorContent').addClass('hidden');

                    if ((data.errors)) {
                        setTimeout(function () {
                            $('#editModal').modal('show');
                            toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                        }, 500);

                        if (data.errors.title) {
                            $('.errorTitle').removeClass('hidden');
                            $('.errorTitle').text(data.errors.title);
                        }
                        if (data.errors.content) {
                            $('.errorContent').removeClass('hidden');
                            $('.errorContent').text(data.errors.content);
                        }
                    } else {
                        toastr.success('Successfully added employee!', 'Success Alert', {timeOut: 5000});
                        $('.item' + data.id).replaceWith("<tr class='item" + 
                            data.id + "'><td>" + 
                            data.id + "</td><td>" + 
                            data.fname + "</td><td>" + 
                            data.lname + "</td><td>" + 
                            data.email + "</td><td>" + 
                            data.contact + 
                            "</td><td class='text-center'><input type='checkbox' class='edit_published' data-id='" + 
                            data.id + 
                            "'></td><td>Right now</td><td><button class='show-modal btn btn-success' data-id='" + 
                            data.id + "' data-fname='" + 
                            data.fname + "' data-lname='" + 
                            data.lname + "'data-email='" + 
                            data.email + "'data-contact='" + 
                            data.contact + 
                            "'><span class='glyphicon glyphicon-eye-open'></span> Show</button> <button class='edit-modal btn btn-info' data-id='" + 
                            data.id + "' data-fname='" + 
                            data.fname + "' data-lname='" + 
                            data.lname + "'data-email='" + 
                            data.email + "'data-contact='" + 
                            data.contact + 
                            "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + 
                            data.id + "' data-fname='" + 
                            data.fname + "' data-lname='" + 
                            data.lname + "'data-email='" + 
                            data.email + "'data-contact='" + 
                            data.contact + "'><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");


                        if (data.is_published) {
                            $('.edit_published').prop('checked', true);
                            $('.edit_published').closest('tr').addClass('warning');
                        }
                        $('.edit_published').iCheck({
                            checkboxClass: 'icheckbox_square-yellow',
                            radioClass: 'iradio_square-yellow',
                            increaseArea: '20%'
                        });
                        $('.edit_published').on('ifToggled', function(event) {
                            $(this).closest('tr').toggleClass('warning');
                        });
                        $('.edit_published').on('ifChanged', function(event){
                            id = $(this).data('id');
                            $.ajax({
                                type: 'POST',
                                url: "{{ URL::route('changeStatus') }}",
                                data: {
                                    '_token': $('input[name=_token]').val(),
                                    'id': id
                                },
                                success: function(data) {
                                    // empty
                                },
                            });
                        });
                    }
                }
            });
        });

        // delete a post
        $(document).on('click', '.delete-modal', function() {
            $('.modal-title').text('Delete');
            $('#id_delete').val($(this).data('id'));
            $('#fname_delete').val($(this).data('fname'));
            $('#lname_delete').val($(this).data('lname'));
            $('#email_delete').val($(this).data('email'));
            $('#contact_delete').val($(this).data('contact'));
            $('#deleteModal').modal('show');
            id = $('#id_delete').val();
        });
        $('.modal-footer').on('click', '.delete', function() {
            $.ajax({
                type: 'DELETE',
                url: 'employee/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {
                    toastr.success('Successfully deleted Post!', 'Success Alert', {timeOut: 5000});
                    $('.item' + data['id']).remove();
                }
            });
        });
    </script>

</body>
</html>