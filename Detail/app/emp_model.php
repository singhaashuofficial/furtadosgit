<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class emp_model extends Model
{
    protected $primarykey ="id";
    protected $table = "employee";
    protected $fillable = array(
    	'fname',
    	'lname',
    	'email',
    	'contact',
    	'attendence'
    );

    public static function getExcerpt($str, $startPos = 0, $maxLength = 50) {
        if(strlen($str) > $maxLength) {
            $excerpt   = substr($str, $startPos, $maxLength - 6);
            $lastSpace = strrpos($excerpt, ' ');
            $excerpt   = substr($excerpt, 0, $lastSpace);
            $excerpt  .= ' [...]';
        } else {
            $excerpt = $str;
        }

        return $excerpt;
    }
}
