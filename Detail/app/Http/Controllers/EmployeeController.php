<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\employee;
use Input;
//use App\Http\Response;
use Response;
use Excel;
use PDF;
use Mail;

class EmployeeController extends Controller
{
     protected $rules =
    [
        'fname'     => 'required|min:2|max:32|regex:/^[a-z ,.\'-]+$/i',
        'lname'     => 'required|min:2|max:32|regex:/^[a-z ,.\'-]+$/i',
        'email'     => 'required|email|min:2|max:128',
        'contact'   => 'required|Numeric|Digits Between:10,10'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $employee = employee::sortable()->get();

        return view('index', ['employee' => $employee]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $employee = new employee();
            $employee->fname = $request->fname;
            $employee->lname = $request->lname;
            $employee->email = $request->email;
            $employee->contact = $request->contact;
            $employee->save();
            return response()->json($employee);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = employee::findOrFail($id);

        return view('employee.show', ['employee' => $employee]);
    }

    public function search(Request $request)
    {
        $fname = $request->fname;
        //var_dump($fname);
        $employee = employee::where('fname',$fname)->sortable()->  get();
        //var_dump($employee);
        return response()->json($employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $employee = employee::findOrFail($id);
            $employee->fname = $request->fname;
            $employee->lname = $request->lname;
            $employee->email = $request->email;
            $employee->contact = $request->contact;
            $employee->save();
            return response()->json($employee);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = employee::findOrFail($id);
        $employee->delete();

        return response()->json($employee);
    }



        public function changeStatus() 
    {
        $id = Input::get('id');

        $employee = employee::findOrFail($id);
        $employee->attendence = !$employee->attendence;
        $employee->save();

        return response()->json($employee);
    }

    public function exportexcelsingle($id) {                         // called when excel need to be exported 
        $results =  employee::where('id',$id)->get();
        $paymentsArray = []; 
        $paymentsArray[] = ['id', 'fname','lname','email','contact','attendence'];

        foreach ($results as $payment) {
        $paymentsArray[] = $payment->toArray();
        }


        Excel::create($id, function($excel) use ($paymentsArray) {

        // Set the spreadsheet title, creator, and description

        // Build the spreadsheet, passing in the payments array
        $excel->sheet('sheet1', function($sheet) use ($paymentsArray) {
            $sheet->fromArray($paymentsArray, null, 'A1', true, true);
        });

        })->download('xlsx');
            
    }

    public function generatepdfsingle($id)          // called when pdf is to be generated for the single data 
    {
        $employee = employee::where('id',$id)->get();

        //if($request->has('download')){
            // Set extra option
            // pass view file
        
            $pdf = PDF::loadView('pdf',['employee' => $employee]);
            return $pdf->download($id.'.pdf'); 

        //}
        return view('index');
    }

        public function generatepdf()                   // called when a pdf is to be generated for the whole data
    {
        $employee = employee::all();

        //if($request->has('download')){
            // Set extra option
            // pass view file
        
            $pdf = PDF::loadView('pdf',['employee' => $employee]);
            return $pdf->download('pdfpreview.pdf'); 

        //}
        return view('form');
    }

    public function exportexcel() {                         // called when excel need to be exported 
        $results =  employee::all();
        $paymentsArray = []; 
        $paymentsArray[] = ['id', 'fname','lname','email','contact'];

        foreach ($results as $payment) {
        $paymentsArray[] = $payment->toArray();
        }


        Excel::create('All_Student_data', function($excel) use ($paymentsArray) {

        // Set the spreadsheet title, creator, and description

        // Build the spreadsheet, passing in the payments array
        $excel->sheet('sheet1', function($sheet) use ($paymentsArray) {
            $sheet->fromArray($paymentsArray, null, 'A1', true, true);
        });

        })->download('xlsx');
            
    }

    public function importExcel()                        // called when excel needs to be imported
        {
        
        if(Input::hasFile('import_file')){
            echo "hello";
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path)->get();
            if(!empty($data) && $data->count()>0){
                //dd($data);
                echo"hi";
                foreach ($data as $key => $value) {
                    echo "hi";
                    $insert[] = ['fname' => $value->fname , 'lname' => $value->lname , 'email' => $value->email , 'contact' => $value->contact];
                }


                    if(!empty($insert)) {
                        DB::table('employee')->insert($insert);
                        dd('INSERTED');
                    }
                }
            
        }
    }

     public function sendmail()                          // called when mail is to be sent
    {
      $data = array('name'=>"Aashu Singh");
   
      Mail::send('index', $data, function($message) {
         $message->to('singhaashu613@gmail.com', 'Furtados')->subject
            ('Laravel Basic Testing Mail');
         $message->from('singhaashu786@gmail.com','Aashu singh');
         //$message->attach('C:\Users\FSM-CAT-19\Downloads\8');
      });
      echo "Basic Email Sent. Check your inbox.";
    }
}