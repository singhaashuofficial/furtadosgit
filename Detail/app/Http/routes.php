<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::resource('employee','EmployeeController');
Route::post('employee/changeStatus', array('as' => 'changeStatus', 'uses' => 'EmployeeController@changeStatus'));

Route::get('/exportexcelsingle/{person}','EmployeeController@exportexcelsingle');

Route::get('/generatepdfsingle/{person}','EmployeeController@generatepdfsingle');

Route::get('/excelgen','EmployeeController@exportexcel');

Route::get('/pdfgen', 'EmployeeController@generatepdf');

Route::any('/importExcel', 'EmployeeController@importExcel');

Route::get('/sendmail', 'EmployeeController@sendmail');

Route::post('/search', 'EmployeeController@search');